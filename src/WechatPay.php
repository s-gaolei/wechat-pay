<?php declare(strict_types=1);

namespace Gaolei\WechatPay;

class WechatPay
{

    use Wechat;

    protected $appId;

    public static function create(array $config): WechatPay
    {
        try {
            $instance = new static();
            $instance->mchId = $config['mchId'];
            $instance->appId = $config['appId'];
            $instance->mchCertNo = $config['mchCertNo'];
            $instance->mchCertPath = $config['mchCertPath'];
            $instance->wxCertPath = $config['wxCertPath'] ?? null;
            $instance->wxPubCertNo = $config['wxPubCertNo'] ?? null;
            $instance->wxPubCertPath = $config['wxPubCertPath'] ?? null;
            if (!$instance->wxCertPath && !$instance->wxPubCertPath) {
                throw new \Exception('平台证书与公钥证书必须配置一个');
            }
            return $instance;
        } catch (\Throwable $t) {
            throw new \Exception($t->getMessage());
        }
    }

    /**
     * 组装基础下单数据
     * @param array $params
     * @return array
     * @author gaolei 2021/7/30 7:47 下午
     */
    protected function getPackage(array $params): array
    {
        return [
            'mchid' => $this->mchId,
            'appid' => $this->appId,
            'out_trade_no' => $params['order'],
            'description' => $params['title'],
            'notify_url' => $params['notify'],
            'attach' => @$params['attach'],
            'goods_tag' => @$params['goods_tag'],
            'scene_info' => @$params['scene_info'],
            'amount' => [
                'total' => $params['amount'] * 100
            ]
        ];
    }

    /**
     * 发起扫码支付
     * @param array $params
     * @return null[]
     * @author gaolei 2021/7/29 6:42 下午
     */
    public function native(array $params): array
    {
        $response = ['error' => null, 'body' => null];
        $package = $this->getPackage($params);
        $expire = $this->getExpire((int)@$params['time_expire']);
        $package['time_expire'] = $expire['date'];
        $package = array_filter($package);
        try {
            $resp = $this->getPay()->chain('v3/pay/transactions/native')->post(['json' => $package]);
            $pack['expire'] = $expire['time'];
            $pack = $pack + json_decode((string)$resp->getBody(), true);
            $response['body'] = $pack;
        } catch (\Throwable $t) {
            $response['error'] = $t->getMessage();
        }
        return $response;
    }

    /**
     * 发起H5支付
     * @param array $params
     * @return null[]
     * @author gaolei 2021/7/29 6:42 下午
     */
    public function h5Pay(array $params): array
    {
        $response = ['error' => null, 'body' => null];
        $package = $this->getPackage($params);
        $expire = $this->getExpire((int)@$params['time_expire']);
        $package['time_expire'] = $expire['date'];
        $package = array_filter($package);
        try {
            $resp = $this->getPay()->chain('v3/pay/transactions/h5')->post(['json' => $package]);
            $pack['expire'] = $expire['time'];
            $pack = $pack + json_decode((string)$resp->getBody(), true);
            $response['body'] = $pack;
        } catch (\Throwable $t) {
            $response['error'] = $t->getMessage();
        }
        return $response;
    }

    /**
     * 发起jsApi 支付
     * @param array $params
     * @return null[]
     * @author gaolei 2021/7/29 6:42 下午
     */
    public function jsPay(array $params): array
    {
        $response = ['error' => null, 'body' => null];
        $package = $this->getPackage($params);
        $package['payer'] = $params['payer'];
        $expire = $this->getExpire((int)@$params['time_expire']);
        $package['time_expire'] = $expire['date'];
        $package = array_filter($package);
        try {
            $resp = $this->getPay()->chain('v3/pay/transactions/jsapi')->post(['json' => $package]);
            $prepay = json_decode((string)$resp->getBody(), true);
            $pack = [
                'appId' => $this->appId,
                'timeStamp' => (string)time(),
                'nonceStr' => self::randomStr(18),
                'package' => "prepay_id=" . $prepay['prepay_id']
            ];
            $pack = array_merge($pack, ['signType' => 'RSA', 'paySign' => $this->generateSign(array_values($pack))]);
            $response['body'] = ['expire' => $expire['time'], 'pkg' => $pack];
        } catch (\Throwable $t) {
            $response['error'] = $t->getMessage();
        }
        return $response;
    }

    /**
     * 发起 App 支付
     * @param array $params
     * @return null[]
     * @author gaolei 2021/7/29 6:42 下午
     */
    public function appPay(array $params): array
    {
        $response = ['error' => null, 'body' => null];
        $package = $this->getPackage($params);
        $expire = $this->getExpire((int)@$params['time_expire']);
        $package['time_expire'] = $expire['date'];
        $package = array_filter($package);
        try {
            /** @var \GuzzleHttp\Psr7\Response $resp */
            $resp = $this->getPay()->chain('v3/pay/transactions/app')->post(['json' => $package]);
            $prepay = json_decode((string)$resp->getBody(), true);
            $pack = [
                'appId' => $this->appId,
                'timeStamp' => (string)time(),
                'nonceStr' => self::randomStr(18),
                'prepayId' => $prepay['prepay_id']
            ];
            $pack = array_merge($pack, [
                'partnerId' => $this->mchId,
                'packageValue' => "Sign=WXPay",
                'sign' => $this->generateSign(array_values($pack))
            ]);
            $response['body'] = ['expire' => $expire['time'], 'pkg' => $pack];
        } catch (\Throwable $t) {
            $response['error'] = $t->getMessage();
        }
        return $response;
    }

    /**
     * 关闭订单
     * @param string $orderNo
     * @return array|null[]
     * @author gaolei 2022/10/18 10:42 上午
     */
    public function close(string $orderNo): array
    {
        $resp = ['error' => null, 'body' => null];
        try {
            /** @var \GuzzleHttp\Psr7\Response $reqResp */
            $reqResp = $this->getPay()->chain('v3/pay/transactions/outTradeNo/{out_trade_no}/close')->post([
                'json' => ['mchid' => $this->mchId], 'out_trade_no' => $orderNo,
            ]);
            $statusCode = $reqResp->getStatusCode();
            if ($statusCode !== 204) {
                $resp['error'] = $statusCode;
                $resp['body'] = (string)$reqResp->getBody();
            }
        } catch (\GuzzleHttp\Exception\ClientException $exception) {
            $response = $exception->getResponse();
            $resp['error'] = $response->getStatusCode();
            $resp['body'] = json_decode((string)$response->getBody() ?: $response->getReasonPhrase(), true);
        } catch (\Throwable $exception) {
            $resp['error'] = 5000;
            $resp['body'] = json_decode($exception->getMessage(), true);
        }
        return $resp;
    }

}
