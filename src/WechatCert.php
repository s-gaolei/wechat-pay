<?php declare(strict_types=1);

namespace Gaolei\WechatPay;

use WeChatPay\Crypto\AesGcm;

class WechatCert
{

    private $mchId;
    private $certNo;
    private $certPath;

    /**
     * 生成微信平台证书
     * @param string $mchId
     * @param string $apiV3Key
     * @param string $certNo
     * @param string $certPath
     * @param string $savePath
     * @return array
     * @throws \Exception
     * @author gaolei 2021/9/8 7:17 下午
     */
    public static function generate(string $mchId, string $apiV3Key, string $certNo, string $certPath, string $savePath = './'): array
    {
        $instances = new self();
        $instances->mchId = $mchId;
        $instances->certNo = $certNo;
        $instances->certPath = $certPath;
        $resp = $instances->getCertificates();
        if(!isset($resp['data'])){
            throw new \Exception('证书文件生成失败！:'. json_encode($resp,JSON_UNESCAPED_UNICODE));
        }
        $certData = $instances->getCertificates()['data'][0];
        $certificate = $certData['encrypt_certificate'];
        $data = AesGcm::decrypt(
            $certificate['ciphertext'], $apiV3Key, $certificate['nonce'], $certificate['associated_data']
        );
        if (substr($savePath, -strlen('.pem')) !== '.pem') {
            $savePath = rtrim($savePath, '/') . "/wechat_cert_{$mchId}.pem";
        }
        if (file_put_contents($savePath, $data) === false) {
            throw new \Exception('证书文件生成失败！');
        }
        $expireTime = WechatPay::getTimeByWxTime($certData['expire_time']);
        return ['expired_at' => $expireTime, 'full_path' => $savePath];
    }

    /**
     * 获取平台证书内容
     * @return mixed
     * @throws \Exception
     * @author gaolei 2021/9/8 6:28 下午
     */
    private function getCertificates()
    {
        $merchant_id = $this->mchId;
        $serial_no = $this->certNo;
        $sign = $this->getSign(
            "https://api.mch.weixin.qq.com/v3/certificates", $this->getPrivateKey(), $merchant_id, $serial_no
        );
        $header[] = 'User-Agent:https://zh.wikipedia.org/wiki/User_agent';
        $header[] = 'Accept:application/json';
        $header[] = 'Authorization:WECHATPAY2-SHA256-RSA2048 ' . $sign;
        $content = $this->httpRequest("https://api.mch.weixin.qq.com/v3/certificates", $header);
        return json_decode($content, true);
    }

    /**
     * 获取sign
     * @param string $url
     * @param $mch_private_key [商户私钥]
     * @param $merchant_id [商户号]
     * @param $serial_no [证书编号]
     * @return string
     */
    private function getSign(string $url, $mch_private_key, $merchant_id, $serial_no): string
    {
        $timestamp = time();
        $nonce = $timestamp . rand(10000, 99999);
        $url_parts = parse_url($url);
        $canonical_url = ($url_parts['path'] . (!empty($url_parts['query']) ? "?${url_parts['query']}" : ""));
        $message = "GET\n{$canonical_url}\n{$timestamp}\n{$nonce}\n\n";
        openssl_sign($message, $raw_sign, $mch_private_key, 'sha256WithRSAEncryption');
        $sign = base64_encode($raw_sign);
        return sprintf('mchid="%s",nonce_str="%s",timestamp="%d",serial_no="%s",signature="%s"',
            $merchant_id, $nonce, $timestamp, $serial_no, $sign);
    }

    /**
     * 获取商户私钥
     * @return false|resource
     */
    private function getPrivateKey()
    {
        return openssl_get_privatekey(file_get_contents($this->certPath));
    }

    /**
     * 数据请求
     * @param string $url
     * @param array $header 获取头部
     * @param string $postData POST数据，不填写默认以GET方式请求
     * @return bool|string
     * @throws \Exception
     */
    private function httpRequest(string $url, array $header = array(), string $postData = "")
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 2);
        if ($postData !== "") {
            curl_setopt($ch, CURLOPT_POST, TRUE);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $postData); //设置post提交数据
        }
        //判断当前是不是有post数据的发
        $output = curl_exec($ch);
        if ($output === FALSE) {
            throw new \Exception("curl 错误信息: " . curl_error($ch));
        }
        curl_close($ch);
        return $output;
    }
}