<?php declare(strict_types=1);

namespace Gaolei\WechatPay;

use WeChatPay\Builder;
use WeChatPay\Crypto\Rsa;
use WeChatPay\Util\PemUtil;

trait Wechat
{

    protected $mchId;
    protected $mchCertNo;
    protected $mchCertPath;
    protected $wxCertPath;
    protected $wxPubCertNo;
    protected $wxPubCertPath;

    protected function getPay(): \WeChatPay\BuilderChainable
    {
        $certs = [];
        if ($this->wxPubCertPath) {
            $certs[$this->wxPubCertNo] = Rsa::from($this->wxPubCertPath, Rsa::KEY_TYPE_PUBLIC);
        } else {
            $wxCert = PemUtil::loadCertificate($this->wxCertPath);
            $certs[PemUtil::parseCertificateSerialNo($wxCert)] = $wxCert;
        }
        return Builder::factory([
            'mchid' => $this->mchId,
            'serial' => $this->mchCertNo,
            'privateKey' => Rsa::from($this->mchCertPath, Rsa::KEY_TYPE_PRIVATE),
            'certs' => $certs
        ]);
    }

    /**
     * 计算超时时间
     * @param int $expire
     * @return array
     * @author gaolei 2021/7/29 6:55 下午
     */
    protected function getExpire(int $expire): array
    {
        if ($expire < 6 || $expire > 30) {
            $expire = 6;
        }
        $expire = strtotime("+{$expire} minute", time());
        $date = sprintf("%sT%s+08:00", date('Y-m-d', $expire), date('H:i:s', $expire));
        return ['time' => $expire, 'date' => $date];
    }

    /**
     * 生成签名
     * @param array $params
     * @return string
     * @author gaolei 2021/7/30 10:23 上午
     */
    protected function generateSign(array $params): string
    {
        $signStr = implode("\n", $params) . "\n";
        $pKeyId = openssl_get_privatekey(file_get_contents($this->mchCertPath));
        openssl_sign($signStr, $raw_sign, $pKeyId, 'sha256WithRSAEncryption');
        return base64_encode($raw_sign);
    }

    /**
     * 生成token
     * @param int $len
     * @return string
     * @author: gaolei 2021/1/14 8:19 下午
     */
    protected static function randomStr(int $len): string
    {
        $baseStr = 'abcdefghjkmnpqrstuvwxyzABCDEFGHJKMNPQRSTUVWXYZ1234567890';
        $code = '';
        $strLen = strlen($baseStr) - 1;
        for ($i = 0; $i < $len; $i++) {
            $code .= substr($baseStr, rand(0, $strLen - 1), 1);
        }
        return strtoupper($code);
    }

    /**
     * 将微信的时间字符串转化为时间戳
     * @param string $wxTime
     * @return false|int
     * @author gaolei 2021/7/30 6:35 下午
     */
    public static function getTimeByWxTime(string $wxTime)
    {
        $payTime = str_replace('+08:00', '', $wxTime);
        $payTime = implode(' ', explode('T', $payTime));
        return strtotime($payTime);
    }

    protected static $KEY_LENGTH_BYTE = 32;
    protected static $AUTH_TAG_LENGTH_BYTE = 16;

    /**
     * 回调报文解密
     * @param string $apiV3Key
     * @param string $associatedData
     * @param string $nonceStr
     * @param string $ciphertext
     * @return null[]
     * @throws \SodiumException
     * @author gaolei 2021/7/30 3:26 下午
     */
    public static function decryptToString(string $apiV3Key, string $associatedData, string $nonceStr, string $ciphertext): array
    {
        $response = ['error' => null, 'package' => null];
        if (strlen($apiV3Key) != static::$KEY_LENGTH_BYTE) {
            $response['error'] = '无效的ApiV3Key，长度应为32个字节';
        }
        $ciphertext = \base64_decode($ciphertext);
        if (empty($response['error']) && strlen($ciphertext) <= static::$AUTH_TAG_LENGTH_BYTE) {
            $response['error'] = 'ciphertext 参数值无效';
        }
        if (empty($response['error'])) {
            // ext-sodium (default installed on >= PHP 7.2)
            if (function_exists('\sodium_crypto_aead_aes256gcm_is_available') && \sodium_crypto_aead_aes256gcm_is_available()) {
                $resource = \sodium_crypto_aead_aes256gcm_decrypt($ciphertext, $associatedData, $nonceStr, $apiV3Key);
                $response['package'] = json_decode($resource, true) ?: $resource;
            } // ext-libsodium (need install libsodium-php 1.x via pecl)
            elseif (function_exists('\Sodium\crypto_aead_aes256gcm_is_available') && \Sodium\crypto_aead_aes256gcm_is_available()) {
                $resource = \Sodium\crypto_aead_aes256gcm_decrypt($ciphertext, $associatedData, $nonceStr, $apiV3Key);
                $response['package'] = json_decode($resource, true) ?: $resource;
            } // openssl (PHP >= 7.1 support AEAD)
            elseif (PHP_VERSION_ID >= 70100 && in_array('aes-256-gcm', \openssl_get_cipher_methods())) {
                $ctext = substr($ciphertext, 0, -self::$AUTH_TAG_LENGTH_BYTE);
                $authTag = substr($ciphertext, -self::$AUTH_TAG_LENGTH_BYTE);
                $resource = \openssl_decrypt($ctext, 'aes-256-gcm', $apiV3Key, \OPENSSL_RAW_DATA, $nonceStr,
                    $authTag, $associatedData);
                $response['package'] = json_decode($resource, true) ?: $resource;
            } else {
                $response['error'] = 'AEAD_AES_256_GCM需要PHP 7.1以上或者安装libsodium-php';
            }
        }
        return $response;
    }

    /**
     * 获取支付回调中的关键信息
     * @param array $params
     * @return array
     * @author gaolei 2021/7/30 6:25 下午
     */
    public static function getPayRespByResource(array $params): array
    {
        $resp = [
            'order_no' => $params['out_trade_no'],            // 商户订单号
            'payer' => $params['payer']['openid'],            // 支付者openID
            'amount_set' => $params['amount']['total'],       // 应付[订单]金额
            'amount_get' => $params['amount']['payer_total'], // 实付[订单]金额
            'trade_state' => $params['trade_state'],          // 交易状态
            'trade_type' => $params['trade_type'],            // 交易类型
        ];
        $resp['trade_time'] = self::getTimeByWxTime($params['success_time']);
        return $resp;
    }

}