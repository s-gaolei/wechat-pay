<?php declare(strict_types=1);

namespace Gaolei\WechatPay;

class WechatRefund
{

    use Wechat;

    protected function __construct()
    {
    }

    public static function create(array $config): WechatRefund
    {
        try {
            $instance = new static();
            $instance->mchId = $config['mchId'];
            $instance->mchCertNo = $config['mchCertNo'];
            $instance->mchCertPath = $config['mchCertPath'];
            $instance->wxCertPath = $config['wxCertPath'] ?? null;
            $instance->wxPubCertNo = $config['wxPubCertNo'] ?? null;
            $instance->wxPubCertPath = $config['wxPubCertPath'] ?? null;
            if (!$instance->wxCertPath && !$instance->wxPubCertPath) {
                throw new \Exception('平台证书与公钥证书必须配置一个');
            }
            return $instance;
        } catch (\Throwable $t) {
            throw new \Exception($t->getMessage());
        }
    }

    /**
     * 发起退款申请
     * @author gaolei 2021/7/29 7:36 下午
     */
    public function refund(array $params): array
    {
        $resp = ['error' => null, 'body' => null];
        $refundParams = [
            'out_trade_no' => $params['order_no'],
            'out_refund_no' => $params['refund_no'],
            'notify_url' => @$params['notify_url'],
            'amount' => [
                'currency' => 'CNY',
                'total' => sprintf("%.2f", $params['order_amount']) * 100,
                'refund' => sprintf("%.2f", $params['refund_amount']) * 100,
            ]
        ];
        if (!empty($params['reason'])) {
            $refundParams['reason'] = $params['reason'];
        }
        $this->getPay()->chain('v3/refund/domestic/refunds')->postAsync(['json' => $refundParams])
            ->then(static function ($response) use (&$resp) {
                // 正常逻辑回调处理
                $content = json_decode($response->getBody()->getContents(), true);
                $respBody = [
                    'refund' => $content['amount']['payer_refund'],
                    'refund_to' => $content['user_received_account'],
                    'channel' => $content['channel'],
                    'create_time' => self::getTimeByWxTime($content['create_time']),
                    'status' => $content['status'],
                ];
                if ($content['status'] === 'SUCCESS') {
                    $respBody['success_time'] = self::getTimeByWxTime($content['success_time']);
                }
                $resp['body'] = $respBody;
            })->otherwise(static function ($exception) use (&$resp) {
                // 异常逻辑回调处理
                $resp['error'] = $exception->getMessage();
            })
            ->wait();
        return $resp;
    }
}
